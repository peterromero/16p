var mbtiState;

$(function () {
    mbtiState = new MBTI({
        $profileListForm: $('#profile_list'),
        $profileListTable: $('table.profile_list'),
        $profileSetKeyModal: $('#profile_set_modal'),
        $profileSetKeyForm: $('#profile_set'),
        $profileSetKeyField: $('#profile_set_key'),
        $profileSetKeyNew: $('a.new-profile-set'),
        $profileSetKeyReadout: $('p.profile-key-readout'),
        $errorDisplay: $('p.error'),
        $canvasContainer: $('.canvas-container'),
        $axisSelectorRow: $('.row.axes')
    });
});
