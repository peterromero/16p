<?php
ini_set('error_reporting', 0);
require_once('bootstrapper.php');
$db = DatabaseConnection::getInstance();

$result = json_encode(ajax_submission_result($_REQUEST));
print $result;

function ajax_submission_result($input) {
	$form_id = $input['formId'];
	parse_str($input['formData'], $data);
	$action = $input['action'];

	if (!$form_id) {
		return NULL;
	}

	$callback_function_name = $form_id . '_form_process';
	$form_response = $callback_function_name($action, $data);
	return [
		'formId' => $form_id,
		'action' => $action,
		'response' => $form_response
	];
}

function profile_set_form_process($action, $data) {
	switch ($action) {
		case 'load_existing': {
			return load_existing_profile_set($data['profile_set_key']);
			break;
		}
		case 'create_new': {
			return create_new_profile_set();
			break;
		}
		default: {
			return NULL;
			break;
		}
	}
}

function get_unique_profile_set_key() {
	global $db;
	do {
		$key = rand(100000, 999999);
		$query = $db->connection->prepare('SELECT count(id) FROM profile_set WHERE profile_set_key=?');
		$query->bind_param('s', $key);
		$query->execute();
		$result = $query->get_result();
		$match_count = $result->fetch_all()[0][0];
	} while ($match_count != 0);

	$key = preg_replace('/(\d{3})(\d{3})/', "$1-$2", $key);

	return $key;
}

function create_new_profile_set() {
	global $db;
	$key = get_unique_profile_set_key();

	$query = $db->connection->prepare('INSERT INTO profile_set (profile_set_key) VALUES (?)');
	$query->bind_param('s', $key);
	$query->execute();

	return [
		'id' => $query->insert_id,
		'key' => $key
	];
}

function get_profile_set_id_from_key($key) {
	global $db;

	$query = $db->connection->prepare('SELECT id FROM profile_set WHERE profile_set_key = ?');
	$query->bind_param('s', $key);
	$query->execute();

	$result = $query->get_result();

	if ($result === false || $result->num_rows === 0) {
		if ($key) {
			return [
				'error' => "$key is not a valid profile set key."
			];
		} else {
			return [
				'error' => "Invalid or malformed profile set key entered."
			];
		}
	}

	$key_id = $result->fetch_all()[0][0];
	return $key_id;
}

function load_existing_profile_set($key) {
	global $db;

	$key_id = get_profile_set_id_from_key($key);
	if (is_array($key_id)) {
		return $key_id;
	}

	$query = $db->connection->prepare('SELECT * FROM profile WHERE profile_set_id = ? ORDER BY id ASC');
	$query->bind_param('i', $key_id);
	$query->execute();

	$result = $query->get_result();

	$profiles = [];
	while ($profile = $result->fetch_assoc()) {
		$profile['code'] = get_profile_code($profile);
		$profiles[] = $profile;
	}

	return [
		'key' => $key,
		'profiles' => $profiles
	];
}

/**
 * Gets the 5-letter code for the given profile; i.e. "INTJ-A".
 * @param array $profile
 * The profile to parse for its code, having the following keys:
 * ei, ns, tf, jp, at
 * @return string
 */
function get_profile_code(array $profile) {
	$letters = [];
	$letters['ei'] = ($profile['ei'] <= 50) ? 'I' : 'E';
	$letters['ns'] = ($profile['ns'] <= 50) ? 'S' : 'N';
	$letters['tf'] = ($profile['tf'] <= 50) ? 'F' : 'T';
	$letters['jp'] = ($profile['jp'] <= 50) ? 'P' : 'J';
	$letters['at'] = ($profile['at'] <= 50) ? 'T' : 'A';

	return $letters['ei'] . $letters['ns'] . $letters['tf'] . $letters['jp'] . '-' . $letters['at'];
}

function profile_list_form_process($action, $data) {
	switch ($action) {
		case 'delete':
			global $db;

			$query = $db->connection->prepare('DELETE FROM profile WHERE id = ?');
			$query->bind_param('i', $data['row_id']);
			$query->execute();

			if (!empty($query->error)) {
				return [
					'status' => FALSE,
					'error' => $query->error
				];
			}
			if ($query->affected_rows == 0) {
				return [
					'status' => FALSE,
					'error' => 'Error: No profiles were deleted.'
				];
			} else {
				return [
					'status' => TRUE,
					'rowId' => $data['row_id']
				];
			}
			break;
		case 'add':
			global $db;

			// Get the profile set id from the key. If there is an error, return
			// it.
			$profile_set_id = get_profile_set_id_from_key($data['profile_set_key']);
			if (is_array($profile_set_id)) {
				return $profile_set_id;
			}

			$query = $db->connection->prepare('INSERT INTO profile (profile_set_id, name, ei, ns, tf, jp, at) VALUES (?, ?, ?, ?, ?, ?, ?)');
			$query->bind_param('isiiiii', $profile_set_id, $data['name'], $data['ei'], $data['ns'], $data['tf'], $data['jp'], $data['at']);
			$query->execute();

			if (!empty($query->error)) {
				return [
					'status' => FALSE,
					'error' => $query->error
				];
			} else {
				unset($data['row_id']);
				$data['code'] = get_profile_code($data);
				return [
					'status' => TRUE,
					'rowId' => $query->insert_id,
					'values' => $data
				];
			}

			break;
		case 'edit':
			global $db;

			$query = $db->connection->prepare('UPDATE `profile` SET `name` = ?, `ei` = ?, `ns` = ?, `tf` = ?, `jp` = ?, `at` = ? WHERE `id` = ?');
			$query->bind_param('siiiiii', $data['name'], $data['ei'], $data['ns'], $data['tf'], $data['jp'], $data['at'], $data['row_id']);
			$query->execute();

			if (!empty($query->error)) {
				return [
					'status' => FALSE,
					'error' => $query->error
				];
			} else {
				$data['code'] = get_profile_code($data);
				return [
					'status' => TRUE,
					'values' => $data
				];
			}

			break;
		default:
			return NULL;
			break;
	}
}
