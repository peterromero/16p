<?php

/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 3/31/2017
 * Time: 3:19 PM
 */
class DatabaseConnection
{
	private function __construct() {
		global $MBTI;
		$this->connection = mysqli_connect(
			$MBTI['db']['host'],
			$MBTI['db']['user'],
			$MBTI['db']['password'],
			$MBTI['db']['database']
		);
	}

	public static function getInstance() {
		return new self();
	}
}
