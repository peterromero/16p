<html>
<head>
    <title>MBTI Visualizer</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i|Raleway:100,400,700"
          rel="stylesheet">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/compass/stylesheets/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="assets/plugins/jquery.maskedinput.js"></script>
    <script src="assets/plugins/cookies.js"></script>
    <script src="assets/plugins/jquery.tablesorter/jquery.tablesorter.js"></script>
    <script src="assets/plugins/vis-4.19.1/dist/vis.js"></script>
    <script src="assets/js/mbti.js"></script>
    <script src="assets/js/main.js"></script>
    <meta name="viewport" content="width=device-width,user-scalable=no">
</head>
<body>
<div id="main">
    <div class="row">
        <h1 class="grid full">Team MBTI Visualizer</h1>
    </div>
    <div class="row">
        <div class="grid full">
            <p class="profile-key-readout"
               title="Load a different set of profiles"></p>
        </div>
    </div>
    <div class="row">
        <div class="grid full">
            <p class="error">Here's an error message!</p>
        </div>
    </div>
    <div class="row">
        <div class="grid half">
            <div class="canvas-container"></div>
        </div>
        <div class="grid half">
            <h2>Profiles <i class="action large add" title="Add New"></i></h2>
            <form id="profile_list">
                <input type="hidden" name="row_id" class="row_id" value="">
                <input type="hidden" name="profile_set_key"
                       class="profile_set_key" value="">
                <table class="profile_list tablesorter">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Name</th>
                        <th>E/I</th>
                        <th>N/S</th>
                        <th>T/F</th>
                        <th>J/P</th>
                        <th>A/T</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <div class="row axes">
        <div class="grid fifth">
            <label for="x-axis">X-Axis Value</label>
            <select name="x-axis" id="x-axis">
                <option value="ei">Extraverted/Introverted</option>
                <option value="ns" selected>Intuitive/Observant</option>
                <option value="tf">Thinking/Feeling</option>
                <option value="jp">Judging/Prospecting</option>
                <option value="at">Assertive/Turbulent</option>
            </select>
        </div>
        <div class="grid fifth">
            <label for="y-axis">Y-Axis Value</label>
            <select name="y-axis" id="y-axis">
                <option value="ei">Extraverted/Introverted</option>
                <option value="ns">Intuitive/Observant</option>
                <option value="tf" selected>Thinking/Feeling</option>
                <option value="jp">Judging/Prospecting</option>
                <option value="at">Assertive/Turbulent</option>
            </select>
        </div>
        <div class="grid fifth">
            <label for="z-axis">Z-Axis Value</label>
            <select name="z-axis" id="z-axis">
                <option value="ei">Extraverted/Introverted</option>
                <option value="ns">Intuitive/Observant</option>
                <option value="tf">Thinking/Feeling</option>
                <option value="jp" selected>Judging/Prospecting</option>
                <option value="at">Assertive/Turbulent</option>
            </select>
        </div>
        <div class="grid fifth">
            <label for="value-axis">Color Axis Value</label>
            <select name="value-axis" id="value-axis">
                <option value="ei" selected>Extraverted/Introverted</option>
                <option value="ns">Intuitive/Observant</option>
                <option value="tf">Thinking/Feeling</option>
                <option value="jp">Judging/Prospecting</option>
                <option value="at">Assertive/Turbulent</option>
            </select>
        </div>
        <div class="grid fifth">
            <label for="no-axis">Do Not Graph</label>
            <select name="no-axis" id="no-axis">
                <option value="ei">Extraverted/Introverted</option>
                <option value="ns">Intuitive/Observant</option>
                <option value="tf">Thinking/Feeling</option>
                <option value="jp">Judging/Prospecting</option>
                <option value="at" selected>Assertive/Turbulent</option>
            </select>
        </div>
    </div>
</div>

<div id="profile_set_modal">
    <form id="profile_set">
        <div class="row">
            <label for="profile_set_key">Enter your profile set key:</label>
        </div>
        <div class="row">
            <input type="text" id="profile_set_key" name="profile_set_key"
                   value="" autocomplete="off">
        </div>
        <div class="row">
            <a href="#" class="new-profile-set">Create a new profile set key</a>
        </div>
        <div class="row">
            <p class="note">The profile set key is a unique identifier you can
                use to view your data again later.<br>You must either create a
                new profile set key or enter an existing key to use the app.</p>
        </div>
    </form>
</div>
</body>
</html>
