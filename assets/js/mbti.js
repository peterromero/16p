var MBTI = function (data) {
    // Plug in various jQuery objects from the page so they are not tightly
    // coupled.
    this.profileSet = new ProfileSet(this);
    this.$profileListForm = data.$profileListForm;
    this.$profileListTable = data.$profileListTable;
    this.$profileSetKeyModal = data.$profileSetKeyModal;
    this.$profileSetKeyForm = data.$profileSetKeyForm;
    this.$profileSetKeyField = data.$profileSetKeyField;
    this.$profileSetKeyNew = data.$profileSetKeyNew;
    this.$profileSetKeyReadout = data.$profileSetKeyReadout;
    this.$errorDisplay = data.$errorDisplay;
    this.$canvasContainer = data.$canvasContainer;
    this.$axisSelectorRow = data.$axisSelectorRow;
    var app = this;

    // Initialize the CRUD state of the two forms on the page.
    this.forms = {
        profileSet: {
            crudState: {
                showEditUi: false,
                pendingAction: false, // false, create, load
                commitChanges: false
            }
        },
        profileList: {
            crudState: {
                showEditUi: false,
                pendingAction: false, // false, add, edit, delete
                commitChanges: false,
                rowId: -1
            }
        }
    };

    /**
     * Determines whether the given key is a valid profile set key in the format
     * 000-000
     * @param key
     * The key to check.
     * @returns {boolean}
     */
    this.isValidProfileSetKey = function (key) {
        if (key) {
            var matches = key.match(/\d{3}-\d{3}/);
            return !!matches;
        }
        else {
            return false;
        }
    };

    /**
     * This function is called whenever the user performs a data-related action
     * on the page, such as adding, deleting, or editing profiles.
     * @param {string} formId
     * The id of the form (as defined by MBTI, not the actual id attribute of the form element). Available options are profileList and profileSet.
     * @param {Object} crudState
     * An object detailing several aspects of the selected action and what it requires.
     * For formId == 'profileList' (which edits the list of profiles):
     * <ul>
     *     <li>pendingAction (string): The name of the action requested. Valid
     *     values are beginAdd (to show the UI), add (to commit the changes),
     *     cancelAdd, beginEdit, edit, cancelEdit, and delete.</li>
     *     <li>showEditUi (boolean): Whether or not this action should include showing the
     *    edit UI on either a new row or an existing row.</li>
     *    <li>commitChanges (boolean): Whether or not to submit the form via
     *    AJAX as part of this operation.</li>
     *    <li>rowId (int): The id of the row to edit/delete. In the case of add
     *    operations, rowId can be -1.</li>
     *    <li>$clickedCell (jQuery Object): When pendingAction is beginEdit,
     *    $clickedCell is passed. It is the jQuery object for the td that was
     *    clicked, and it is used to inform the UI which cell in the edit UI to
     *    focus.</li>
     * </ul>
     * For formId == 'profileSet' (which is the modal form that edits which set
     * of profiles we're viewing):
     * <ul>
     *     <li>pendingAction (string): The name of the action requested. Valid
     *     values are create_new and load_existing.</li>
     *     <li>showEditUi (boolean): Setting to true will show the modal screen
     *     to select a new profile set. False hides it.</li>
     *     <li>commitChanges (boolean): Whether or not to submit the form via
     *    AJAX as part of this operation.</li>
     * </ul>
     */
    this.setCrudState = function (formId, crudState) {
        this.forms[formId].crudState = crudState;
        if (formId === 'profileList') {
            switch (this.forms[formId].crudState.pendingAction) {
                case 'edit':
                case 'delete':
                    app.$profileListForm.find('.row_id').val(this.forms[formId].crudState.rowId);
                    break;
                default:
                    break;
            }
            if (this.forms[formId].crudState.commitChanges) {
                app.ajaxFormSubmit(app.$profileListForm, this.forms[formId].crudState.pendingAction);
            }
            this.updateProfileTable();
        }
        if (formId === 'profileSet') {
            if (this.forms[formId].crudState.showEditUi) {
                this.showProfileSetModal();
            }
            else {
                this.hideProfileSetModal();
            }
            if (this.forms[formId].crudState.commitChanges) {
                app.ajaxFormSubmit(app.$profileSetKeyForm, this.forms[formId].crudState.pendingAction);
            }
        }
    };

    /**
     * Displays an error message on the screen.
     * @param message
     * The message to display.
     */
    this.setError = function (message) {
        this.$errorDisplay.html(message).slideDown(300);
    };

    /**
     * Submit an on-page form via AJAX.
     * @param $form
     * The jQuery form object to submit.
     * @param action
     * A string specifying which action to perform. This will vary per form.
     * Look in ajax.php for details.
     */
    this.ajaxFormSubmit = function ($form, action) {
        var formId = $form.attr('id');
        var formData = $form.serialize();
        this.wait(true);
        $.ajax({
            url: '/ajax.php',
            data: {
                formId: formId,
                formData: formData,
                action: action
            },
            dataType: 'json'
        }).done(function (data) {
            app.handleFormReturns(data);
            app.wait(false);
        });
    };

    /**
     * Shows the modal window where users input or generate a new profile set
     * key.
     */
    this.showProfileSetModal = function () {
        $('#main').addClass('inactive');
        this.$profileSetKeyModal.fadeIn(400, function () {
            app.$profileSetKeyField.focus();
        });
    };

    /**
     * Hides the modal window where users input or generate a new profile set
     * key.
     */
    this.hideProfileSetModal = function () {
        $('#main').removeClass('inactive');
        this.$profileSetKeyModal.fadeOut(400);
    };

    /**
     * Puts the application in or out of 'wait' mode (which is just changing the
     * cursor on the screen so users know not to click).
     * @param {boolean} state
     * True puts the app into wait mode; false takes it out.
     */
    this.wait = function (state) {
        if (state) {
            $('body').addClass('waiting');
        }
        else {
            $('body').removeClass('waiting');
        }
    };

    // INITIALIZATION
    // Check cookies for current application state.
    var key = readCookie('key');
    if (!this.isValidProfileSetKey(key)) {
        // If there is no cookie storing a profile set key, show the modal for
        // the user to input one or create a new one.
        this.setCrudState('profileSet', {
            showEditUi: true,
            pendingAction: false,
            commitChanges: false
        });
    }
    else {
        // If there is a cookie, load the specified profile set, bypassing the
        // modal.
        this.$profileSetKeyField.val(key);
        this.setCrudState('profileSet', {
            showEditUi: false,
            pendingAction: 'load_existing',
            commitChanges: true
        });
    }

    // Initialize table sorter.
    this.$profileListTable.tablesorter({
        headers: {
            0: {sorter: false},
            7: {sorter: false}
        }
    });

    // Do not allow standard (HTML) form submissions.
    $('form').submit(function (event) {
        event.preventDefault();
    });

    // Close error messages on click.
    $('.error').click(function () {
        $(this).slideUp(300);
    });

    // Resize graph when window changes size.
    $(window).resize(function () {
        var width = app.$canvasContainer.width();
        var height = app.$canvasContainer.css('padding-bottom');

        app.profileSet.graph.setOptions({
            width: width,
            height: height
        });
        app.profileSet.redraw();
    });

    // Store initial axis selector values in $.data.
    this.$axisSelectorRow.find('select').each(function () {
        $(this).data('previousValue', $(this).val());
    });

    // Shift axis values around when one is changed.
    this.$axisSelectorRow.find('select').change(function () {
        var previousValue;
        previousValue = $(this).data('previousValue');
        var newValue = $(this).val();
        app.$axisSelectorRow.find('select').each(function () {
            if ($(this).data('previousValue') === newValue) {
                $(this).val(previousValue).data('previousValue', previousValue);
            }
        });
        $(this).data('previousValue', newValue);
        app.profileSet.triggerDataUpdate();
    });

    // Profile list CRUD:
    // When the Add button is clicked
    $('h2 i.action.add').click(function () {
        app.setCrudState('profileList', {
            showEditUi: true,
            pendingAction: 'beginAdd',
            commitChanges: false,
            rowId: -1
        });
    });
    // When the cancel button is clicked in the row add UI
    this.$profileListTable.delegate('i.action.cancel.beginAdd', 'click', function () {
        app.setCrudState('profileList', {
            showEditUi: false,
            pendingAction: 'cancelAdd',
            commitChanges: false,
            rowId: -1
        });
    });
    // When the cancel button is clicked in the row edit UI
    this.$profileListTable.delegate('i.action.cancel.beginEdit', 'click', function () {
        app.setCrudState('profileList', {
            showEditUi: false,
            pendingAction: 'cancelEdit',
            commitChanges: false,
            rowId: -1
        });
    });
    // When the delete button is clicked on a row
    this.$profileListTable.delegate('i.action.delete', 'click', function (event) {
        app.setCrudState('profileList', {
            showEditUi: false,
            pendingAction: 'delete',
            commitChanges: true,
            rowId: $(event.target).parents('tr').attr('data-id')
        });
    });
    // When a key is pressed in a field in the row add/edit UI
    this.$profileListTable.delegate('input', 'keydown', function (event) {
        if (event.which === 13) {
            // Return pressed
            var $parentRow = $(this).parents('tr');
            switch ($parentRow.data('action')) {
                case 'add':
                    app.setCrudState('profileList', {
                        showEditUi: false,
                        pendingAction: 'add',
                        commitChanges: true,
                        rowId: -1
                    });
                    break;
                case 'edit':
                    app.setCrudState('profileList', {
                        showEditUi: false,
                        pendingAction: 'edit',
                        commitChanges: true,
                        rowId: $parentRow.data('id')
                    });
                    break;
            }
        }
        if (event.which === 27) {
            // Escape pressed - routes to clicking the cancel button on the row
            $(this).parents('tr').find('i.action.cancel').click();
        }
    });
    // When a field in the static table is clicked, bring up the edit UI for
    // that row.
    this.$profileListTable.delegate('td.editable', 'click', function () {
        app.setCrudState('profileList', {
            showEditUi: true,
            pendingAction: 'beginEdit',
            commitChanges: false,
            $clickedCell: $(this),
            rowId: $(this).parents('tr').attr('data-id')
        });
    });

    // Profile set key CRUD:
    // Enforce formatting for the profile set key input field.
    this.$profileSetKeyField.mask('999-999');
    // When a key is pressed in the profile set key input field
    this.$profileSetKeyField.keydown(function (event) {
        if (event.which === 13) {
            // Return pressed; load specified set
            app.setCrudState('profileSet', {
                showEditUi: false,
                pendingAction: 'load_existing',
                commitChanges: true
            });
        }
    });
    // When the Create New Profile Set link is clicked
    this.$profileSetKeyNew.click(function (event) {
        event.preventDefault();
        app.setCrudState('profileSet', {
            showEditUi: false,
            pendingAction: 'create_new',
            commitChanges: true
        });
    });
    // When the profile set key number is clicked from within the main part of
    // the app, show the modal.
    this.$profileSetKeyReadout.click(function () {
        app.showProfileSetModal();
    });

    /**
     * Updates the display of the profile list table by either showing or hiding
     * the row add/edit UI.
     */
    this.updateProfileTable = function () {
        // Grab the form's CRUD object to save lookups.
        var crud = this.forms.profileList.crudState;

        if (crud.showEditUi) {
            // Add the editing class to the table (once only). This grays out
            // the rest of the content and removes the delete buttons.
            if (!this.$profileListTable.hasClass('editing')) {
                this.$profileListTable.addClass('editing');

                // An array of cell names to iterate through later.
                var cellNames = ['name', 'ei', 'ns', 'tf', 'jp', 'at'];

                // Default field values for the editing interface.
                var name = '', ei = '', ns = '', tf = '', jp = '', at = '';

                // The name of the input that will get focus when the edit
                // interface is displayed.
                var cellToFocus = 'name';

                // The id of the row we're editing. -1 means we're adding a row.
                var id = -1;

                // If we are beginning an edit action, the cells must have the
                // existing row's values in them to start.
                if (crud.pendingAction === 'beginEdit') {
                    var $pendingEditRow = this.$profileListTable.find('tr[data-id=' + crud.rowId + ']');
                    name = $pendingEditRow.find('.name').html();
                    ei = $pendingEditRow.find('.ei').html();
                    ns = $pendingEditRow.find('.ns').html();
                    tf = $pendingEditRow.find('.tf').html();
                    jp = $pendingEditRow.find('.jp').html();
                    at = $pendingEditRow.find('.at').html();

                    id = $pendingEditRow.data('id');

                    // Figure out which cell we need to set the focus to.
                    $.each(cellNames, function (i, value) {
                        if (crud.$clickedCell.hasClass(value)) {
                            cellToFocus = value;
                        }
                    });
                }

                // The addition of this class will be used later to set the
                // focus to the correct cell.
                var gf = 'class="grab-focus" ';

                // Set what action will happen when the user presses enter.
                var enterAction;
                if (crud.pendingAction === 'beginAdd') {
                    enterAction = 'add';
                }
                if (crud.pendingAction === 'beginEdit') {
                    enterAction = 'edit';
                }

                // Create the row that contains all the editing cells.
                var editRow = '<tr class="edit-ui" data-action="' + enterAction + '" data-id="' + id + '">' +
                    '<td><i class="action small cancel ' + crud.pendingAction + '" title="Cancel changes"></i></td>' +
                    '<td><input type="text" name="name" ' + ((cellToFocus === 'name') ? gf : '') + 'id="break-lastpass-search" value="' + name + '"></td>' +
                    '<td><input type="number" min="0" max="100" name="ei" ' + ((cellToFocus === 'ei') ? gf : '') + 'value="' + ei + '"></td>' +
                    '<td><input type="number" min="0" max="100" name="ns" ' + ((cellToFocus === 'ns') ? gf : '') + 'value="' + ns + '"></td>' +
                    '<td><input type="number" min="0" max="100" name="tf" ' + ((cellToFocus === 'tf') ? gf : '') + 'value="' + tf + '"></td>' +
                    '<td><input type="number" min="0" max="100" name="jp" ' + ((cellToFocus === 'jp') ? gf : '') + 'value="' + jp + '"></td>' +
                    '<td><input type="number" min="0" max="100" name="at" ' + ((cellToFocus === 'at') ? gf : '') + 'value="' + at + '"></td>' +
                    '<td></td>' +
                    '</tr>';

                // If we're adding, the row is appended to the end of the table.
                if (crud.pendingAction === 'beginAdd') {
                    this.$profileListTable.find('tbody').append($(editRow)).ready(function () {
                        $('.grab-focus').select().focus();
                    });
                }
                // If we're editing, the row replaces the existing, static row
                // in the table. The static row is stored in the table's data
                // property to be retrieved later in case the user cancels the
                // action.
                if (crud.pendingAction === 'beginEdit') {
                    this.$profileListTable.data('edit-pending-row', $pendingEditRow.replaceWith(editRow)).ready(function () {
                        $('.grab-focus').select().focus();
                    });
                }
            }
        }
        else {
            // If, instead, we need to hide the editing UI,
            switch (crud.pendingAction) {
                case 'cancelEdit':
                    // for edits, swap the existing static row back in.
                    $('tr.edit-ui').replaceWith(this.$profileListTable.data('edit-pending-row'));
                    break;
                case 'add':
                case 'cancelAdd':
                    // For add operations, we can just delete the UI row.
                    $('tr.edit-ui').remove();
                    break;
                default:
                    break;
            }
            // Remove the class which grays out the other table rows.
            this.$profileListTable.removeClass('editing');
        }
    };

    /**
     * A single place to handle all AJAX form returns.
     * @param {Object} data
     * This is the object returned from the AJAX request. It will always contain
     * formId, action, and response. formId is the HTML id attribute of the
     * form. action is the name of the action performed (which is what is passed
     * to setCrudState). response will contain different elements depending on
     * the function call.
     */
    this.handleFormReturns = function (data) {
        switch (data.formId) {
            case 'profile_set':
                if (data.response.error) {
                    this.setError(data.response.error);
                }
                else {
                    switch (data.action) {
                        case 'create_new':
                        case 'load_existing':
                            this.profileSet.load(data.response.key);
                            this.hideProfileSetModal();
                            // Load the selected profiles into the table.
                            this.populateProfileList(data.response.profiles);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 'profile_list':
                if (data.response.status === false) {
                    this.setError(data.response.error);
                }
                else {
                    switch (data.action) {
                        case 'delete':
                            // Remove the record from internal storage.
                            this.profileSet.profiles.splice(data.response.rowId, 1);
                            this.profileSet.triggerDataUpdate();

                            // Fade out the row we just deleted and remove it
                            // from the table.
                            this.$profileListForm.find('tr[data-id=' + data.response.rowId + ']').fadeOut(200, function () {
                                $(this).remove();
                            });
                            this.$profileListTable.trigger('update');
                            break;
                        case 'edit':
                            // Update the row in internal storage.
                            // debugger;
                            this.profileSet.profiles[data.response.values.row_id].updateData(data.response.values);

                            // Get the old static row we stashed away when we
                            // started editing.
                            var $staticRow = app.$profileListTable.data('editPendingRow');

                            // Swap in the new data values.
                            $staticRow.find('.name').html(data.response.values.name);
                            $staticRow.find('.ei').html(data.response.values.ei);
                            $staticRow.find('.ns').html(data.response.values.ns);
                            $staticRow.find('.tf').html(data.response.values.tf);
                            $staticRow.find('.jp').html(data.response.values.jp);
                            $staticRow.find('.at').html(data.response.values.at);
                            $staticRow.find('.code').html(data.response.values.code);

                            // Swap in the static row for the edit row.
                            $('tr.edit-ui').replaceWith(app.$profileListTable.data('editPendingRow'));
                            this.$profileListTable.trigger('update');

                            break;
                        case 'add':
                            // Store the profile internally.
                            this.profileSet.profiles[data.response.rowId] = new Profile(this.profileSet, data.response.values);
                            this.profileSet.triggerDataUpdate();

                            // Create a new row with our data values and append
                            // it to the table.
                            var row = '<tr data-id="' + data.response.rowId + '">' +
                                '<td><i class="action small delete" title="Delete profile"></i></td>' +
                                '<td class="editable name">' + data.response.values.name + '</td>' +
                                '<td class="editable ei">' + data.response.values.ei + '</td>' +
                                '<td class="editable ns">' + data.response.values.ns + '</td>' +
                                '<td class="editable tf">' + data.response.values.tf + '</td>' +
                                '<td class="editable jp">' + data.response.values.jp + '</td>' +
                                '<td class="editable at">' + data.response.values.at + '</td>' +
                                '<td class="code">' + data.response.values.code + '</td>' +
                                '</tr>';
                            this.$profileListTable.find('tbody').append($(row)).trigger('update');
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    };

    /**
     * Empty the profile list table and refill it with fresh data. Also store
     * the data internally.
     * @param profiles
     * An array of profile Objects, each with the following keys:
     * id, name, et, ns, tf, jp, at, code, profile_set_id, color (not used)
     */
    this.populateProfileList = function (profiles) {
        this.$profileListTable.find('tbody').empty();
        this.profileSet.profiles = [];
        var row;
        $.each(profiles, function () {
            app.profileSet.profiles[this.id] = new Profile(app.profileSet, this);
            row = '<tr data-id="' + this.id + '">' +
                '<td><i class="action small delete" title="Delete profile"></i></td>' +
                '<td class="editable name">' + this.name + '</td>' +
                '<td class="editable ei">' + this.ei + '</td>' +
                '<td class="editable ns">' + this.ns + '</td>' +
                '<td class="editable tf">' + this.tf + '</td>' +
                '<td class="editable jp">' + this.jp + '</td>' +
                '<td class="editable at">' + this.at + '</td>' +
                '<td class="code">' + this.code + '</td>' +
                '</tr>';
            app.$profileListTable.find('tbody').append($(row));
        });

        this.$profileListTable.trigger('update');
        this.profileSet.plot();
    };

    /**
     * Returns an array indicating which data column should be mapped to which axis in the graph.
     * @returns {Object}
     */
    this.getAxisColumnMappings = function () {
        var data = {};
        data.x = this.$axisSelectorRow.find('select#x-axis').val();
        data.y = this.$axisSelectorRow.find('select#y-axis').val();
        data.z = this.$axisSelectorRow.find('select#z-axis').val();
        data.value = this.$axisSelectorRow.find('select#value-axis').val();
        data.xLabel = data.x.substr(0, 1).toUpperCase() + '/' + data.x.substr(1, 1).toUpperCase();
        data.yLabel = data.y.substr(0, 1).toUpperCase() + '/' + data.y.substr(1, 1).toUpperCase();
        data.zLabel = data.z.substr(0, 1).toUpperCase() + '/' + data.z.substr(1, 1).toUpperCase();
        data.valueLabel = data.value.substr(0, 1).toUpperCase() + '/' + data.value.substr(1, 1).toUpperCase();
        return data;
    };
};

var ProfileSet = function (app) {
    this.app = app;
    this.key = '00000000';
    this.profiles = [];

    /**
     * Plugs the profile set key into a couple of fields and display area, and
     * stores its value in a cookie.
     * @param key
     */
    this.load = function (key) {
        this.key = key;

        this.app.$profileSetKeyField.val(key);
        this.app.$profileSetKeyReadout.html(key);
        this.app.$profileListForm.find('.profile_set_key').val(key);

        createCookie('key', key, 10);
    };

    this.plot = function () {
        if (!this.profiles || !this.profiles.length) {
            return;
        }

        // Create and populate a data table.
        var data = new vis.DataSet();

        var axisMappings = app.getAxisColumnMappings();

        $.each(this.profiles, function (id, value) {
            if (value) {
                data.add({
                    x: value[axisMappings.x],
                    y: value[axisMappings.y],
                    z: value[axisMappings.z],
                    style: value[axisMappings.value],
                    name: value.name
                });
            }
        });

        var width = app.$canvasContainer.width();
        var height = app.$canvasContainer.css('padding-bottom');

        // specify options
        var options = {
            style: 'dot-color',
            dataColor: {
                fill: 'rgba(255,255,255,0.7)'
            },
            gridColor: '#00aac9',
            axisColor: 'rgba(255,255,255,0.5)',
            xLabel: axisMappings.xLabel,
            xMax: 100,
            xMin: 0,
            yLabel: axisMappings.yLabel,
            yMax: 100,
            yMin: 0,
            zLabel: axisMappings.zLabel,
            zMax: 100,
            zMin: 0,
            valueMin: 0,
            valueMax: 100,
            tooltip: function (data) {
                return data.data.name;
            },
            tooltipStyle: {
                content: {
                    padding: '10px',
                    border: '1px solid rgba(255,255,255,0.3)',
                    color: 'rgba(255,255,255,0.7)',
                    background: 'rgba(0,170,201,0.25)',
                    borderRadius: '6px',
                    boxShadow: 'none',
                    font: 'bold 12px/12px Raleway'
                },
                line: {
                    height: '40px',
                    width: '0',
                    borderLeft: '1px solid rgba(255,255,255,0.3)'
                },
                dot: {
                    height: '0',
                    width: '0',
                    border: '5px solid #ffffff',
                    borderRadius: '5px'
                }
            },
            verticalRatio: 1,
            dotSizeRatio: 0.015,
            legendLabel: axisMappings.valueLabel,
            cameraPosition: {
                horizontal: 0,
                vertical: 0
            },
            width: width,
            height: height,
            showPerspective: false,
            showGrid: true,
            keepAspectRatio: true
        };

        // Plot the graph.
        var container = app.$canvasContainer.get(0);
        this.graph = new vis.Graph3d(container, data, options);
    };

    this.redraw = function () {
        if (typeof this.graph !== 'undefined') {
            this.graph.redraw();
        }
    };

    /**
     * Call this function when the chart data has been changed and it needs to
     * be redrawn.
     */
    this.triggerDataUpdate = function () {
        if (typeof this.graph !== 'undefined') {
            var axisMappings = app.getAxisColumnMappings();
            var data = new vis.DataSet();
            $.each(this.profiles, function (id, value) {
                if (value) {
                    data.add({
                        x: value[axisMappings.x],
                        y: value[axisMappings.y],
                        z: value[axisMappings.z],
                        style: value[axisMappings.value],
                        name: value.name
                    });
                }
            });
            this.graph.setData(data);
            this.graph.setOptions({
                xLabel: axisMappings.xLabel,
                yLabel: axisMappings.yLabel,
                zLabel: axisMappings.zLabel,
                legendLabel: axisMappings.valueLabel
            });
            this.redraw();
        }
    };
};

var Profile = function (profileSet, data) {
    this.updateData = function (data) {
        if (typeof data.name !== 'undefined') {
            this.name = data.name;
        }
        if (typeof data.ei !== 'undefined') {
            this.ei = data.ei;
        }
        if (typeof data.ns !== 'undefined') {
            this.ns = data.ns;
        }
        if (typeof data.tf !== 'undefined') {
            this.tf = data.tf;
        }
        if (typeof data.jp !== 'undefined') {
            this.jp = data.jp;
        }
        if (typeof data.at !== 'undefined') {
            this.at = data.at;
        }
        if (typeof data.code !== 'undefined') {
            this.code = data.code;
        }
        if (typeof data.color !== 'undefined') {
            this.color = data.color;
        }

        this.profileSet.triggerDataUpdate();
    };

    this.profileSet = profileSet;
    this.updateData(data);
};
